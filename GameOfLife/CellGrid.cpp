#include "CellGrid.h"
#include <sstream>

//create cell grid with given size [x][y]
//set each cells position with math (some trial and error to get this right)
GameOfLife::CellGrid::CellGrid(unsigned int x, unsigned int y,unsigned int side):width(x), height(y),PAD(1),sideLength(side)
{
	generations = 0;
	cellGrid.resize(width);
	for(unsigned int i = 0; i < width; ++i)
	{
		//cellGrid[i].resize(height);
		for(unsigned int j = 0; j < height; ++j)
		{
			cellGrid[i].push_back(Cell(sideLength));
			
			////set the position of cell based on its row/col location PAD simply gives a 4 pixel border around grid
			cellGrid[i][j].setPosition(sf::Vector2f((float)(i*sideLength + (i+PAD)), (float)(j*sideLength + (j+PAD))));			
		}
	}	
}

//draw grid by passing RenderTarget to the drawable Cell
void GameOfLife::CellGrid::draw(sf::RenderTarget& target, sf::RenderStates states)
{
	for(unsigned int i = 0; i < width; ++i)
	{
		for(unsigned int j = 0; j < height; ++j)
			cellGrid[i][j].draw(target,states);
	}
}

//before checking if a cell is alive, 
//check if its within bounds
bool GameOfLife::CellGrid::isValidCell(unsigned int i, unsigned int j)
{
	if(/*i < 0 ||*/ i > width-1 || /*j < 0 ||*/ j > height-1)
		return false;
	return true;
}

/** Rules:
* 1. Any live cell with fewer than two live neighbours dies, as if caused by under-population.
* 2. Any live cell with two or three live neighbours lives on to the next generation.
* 3. Any live cell with more than three live neighbours dies, as if by overcrowding.
* 4. Any dead cell with exactly three live neighbours becomes a live cell, as if by reproduction.
*/
void GameOfLife::CellGrid::runGeneration()
{
	std::vector<Cell*> changes;//hold all cells that need to be born or die
	
	for(unsigned int i = 0; i < width; ++i)
	{
		for(unsigned int j = 0; j < height; ++j)
		{
			int neighbors = countNeighbors(i, j);
					
			//I dont generally like these kinds of compound conditions
			//but I do think it makes some sense here
			if((neighbors < 2 && cellGrid[i][j].isAlive())//rule 1
			|| (neighbors > 3 && cellGrid[i][j].isAlive())//rule 3
			|| (neighbors == 3 && !(cellGrid[i][j].isAlive())))//rule 4
			{
				//Cell* cell = &cellGrid[i][j];
				changes.push_back(&cellGrid[i][j]);				
			}
			//rule 2, do nothing					
		}
	}

	for(unsigned int i = 0; i < changes.size(); ++i)
		changes[i]->changeLife();

	++generations;
}

//change a cells state (life) given the cells coord (from mouse click) relative to the window
//have to undo the math used to set its position (ctor and setGridSize)
void GameOfLife::CellGrid::changeCellState(sf::Vector2i& pos)
{		
	unsigned int i = (pos.x - PAD)/(sideLength + 1);
	unsigned int j = (pos.y - PAD)/(sideLength + 1);
			
	if(i < width && j < height)
	{
		cellGrid[i][j].changeLife();
	}
}

//count a cells living neighbors
const int GameOfLife::CellGrid::countNeighbors(int row, int col)
{
	int count = 0;

	for(int i = -1; i <= 1; i++)
	{
		for(int j = -1; j <= 1; j++)
		{
			if(isValidCell(row + i,col + j)) //cell is in bounds
			{
				if(cellGrid[row + i][col + j].isAlive()) //cell is alive
				{
					if((i != 0) || (j != 0))++count;//cell isnt me
				}
			}
		}
	}
	return count;
}

//sets/resets grid to specified size with cels side X side
void GameOfLife::CellGrid::setGridSize(unsigned int x, unsigned int y, unsigned int side)
{	
	if(x > width)
			cellGrid.resize(x);
	for(unsigned int i = 0; i < x; ++i)
	{		
		for(unsigned int j = 0; j < y; ++j)
		{
			if(i >= width || j >= height)
				cellGrid[i].push_back(Cell(side));
			
			cellGrid[i][j].setSize(side);
			cellGrid[i][j].setPosition(sf::Vector2f((float)(i*side + (i+PAD)), (float)(j*side + (j+PAD))));		
		}
	}
	width = x;
	height = y;
	sideLength = side;
}

//apply a preset pattern
void GameOfLife::CellGrid::applyPattern(Patterns::Pattern p)
{	
	generations = 0;
	clearGrid();

	for(unsigned int i = 0; i < Patterns::patterns[p].size(); i+=2)
	{
		cellGrid[width/2 + Patterns::patterns[p][i]][height/2 + Patterns::patterns[p][i+1]].changeLife();
	}
}

std::string GameOfLife::CellGrid::getGenerations()
{
	std::string s;
	std::stringstream ss;
	ss << generations;
	s = ss.str();
	return s;
}

//kill all cells
void GameOfLife::CellGrid::clearGrid()
{
	for(unsigned int i = 0; i < width; ++i)
	{
		for(unsigned int j =0; j < height; ++j)
		{
			if(cellGrid[i][j].isAlive())
				cellGrid[i][j].changeLife();
		}
	}
}