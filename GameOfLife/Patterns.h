/**
*
*
*/
#ifndef GOL_PATTERNS_SV
#define GOL_PATTERNS_SV
#pragma once
#include <vector>

namespace GameOfLife
{
	class Patterns
	{
	public:
		static enum Pattern {CLEAR, GLIDER, /* SMEXPLODER,*/ LGEXPLODER, ROW10, SPACESHIP, TUMBLER, GROWER10, GLIDERGUNA, GLIDERGUNB};	
		static std::vector<std::vector<int> > patterns;

		//initialize static vector with preset patterns
		static void set();
	};

	
}
#endif
