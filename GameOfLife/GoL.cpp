////////////////////////////////////////////////////////////
// Headers
////////////////////////////////////////////////////////////
#include <Windows.h>
#include "resource.h"
#include "CellGrid.h"
using namespace GameOfLife;
////////////////////////////////////////////////////////////
LRESULT CALLBACK OnEvent(HWND Window, UINT Message, WPARAM WParam, LPARAM LParam);

//I dont like globals, but I dont know 
//of a better way to have access to these in the callback
bool run;
CellGrid* grid;
HBRUSH hBrush;

////////////////////////////////////////////////////////////
//GameOfLife entry point
////////////////////////////////////////////////////////////
INT WINAPI WinMain(HINSTANCE Instance, HINSTANCE, LPSTR, INT nCmdShow)
{	
	hBrush = CreateSolidBrush(RGB(230,230,230));

    // Define a class for our main window
    WNDCLASS WindowClass;
    WindowClass.style         = 0;
    WindowClass.lpfnWndProc   = (WNDPROC)OnEvent;
    WindowClass.cbClsExtra    = 0;
    WindowClass.cbWndExtra    = 0;
    WindowClass.hInstance     = Instance;
	WindowClass.hIcon         = LoadIcon(Instance,MAKEINTRESOURCE(IDI_ICON1));
    WindowClass.hCursor       = 0;
    WindowClass.hbrBackground = hBrush;//reinterpret_cast<HBRUSH>(COLOR_BACKGROUND);
    WindowClass.lpszMenuName  = NULL;
    WindowClass.lpszClassName = "SFML App";
    RegisterClass(&WindowClass);

	// Let's create the main window
    HWND Window = CreateWindow("SFML App", 
						"Yet Another Game Of Life Clone - by naraku9333",
						WS_SYSMENU | WS_VISIBLE,
						100, 100, 805, 700, NULL, NULL, 
						Instance,
						NULL);
	
	//set SFML rendertarget to the child window
    sf::RenderWindow window(GetDlgItem(Window, IDS_SFML_STATIC));
	sf::RenderStates states;

	run = false;
	ShowWindow(Window,nCmdShow);	
	
    // Loop until a WM_QUIT message is received
    MSG Message;
    Message.message = ~WM_QUIT;
    while (Message.message != WM_QUIT)
    {		
        if (PeekMessage(&Message, NULL, 0, 0, PM_REMOVE))
        {
            // If a message was waiting in the message queue, process it
            TranslateMessage(&Message);
            DispatchMessage(&Message);
        }
        else
        {
			// Clear views           
            window.clear();
                         
			if(sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				//get mouse position to get the cell clicked on to change it's state
				sf::Vector2i pos = sf::Mouse::getPosition(window);
				grid->changeCellState(pos);
			}
			if(run)
			{
				//run a generation and update label
				grid->runGeneration();				
				SendDlgItemMessage(Window, IDC_GENS_LABEL, WM_SETTEXT, NULL, (LPARAM)grid->getGenerations().c_str());
			}
			grid->draw(window, states);
			window.display();
			
			sf::sleep(sf::seconds(.05f));
		}
    }

	delete grid;
	DeleteObject(hBrush);
    // Destroy the main window
    DestroyWindow(Window);

    // Don't forget to unregister the window class
    UnregisterClass("SFML App", Instance);	

    return EXIT_SUCCESS;
}

////////////////////////////////////////////////////////////
/// Function called whenever one of our windows receives a message
////////////////////////////////////////////////////////////
LRESULT CALLBACK OnEvent(HWND Window, UINT Message, WPARAM WParam, LPARAM LParam)
{
    switch (Message)
    {
		//create all child windows
		case WM_CREATE :
		{
			grid = new CellGrid(132, 99, 5);
			Patterns::set();

			HWND lblPatterns = CreateWindow("STATIC",
									"Select a pattern:",
									WS_CHILD | WS_VISIBLE,
									30, 605, 250, 40,
									Window,
									NULL,
									GetModuleHandle(NULL),
									NULL);
			//some combo boxes
			HWND cboPatterns = CreateWindow("COMBOBOX",
									"cboPatterns",
									WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_VSCROLL | CBS_DROPDOWNLIST,
									30, 625, 250, 180,
									Window,
									(HMENU)IDD_PATTERNS_COMBO,
									GetModuleHandle(NULL),
									NULL);

			HWND lblSize = CreateWindow("STATIC",
									"Select size:",
									WS_CHILD | WS_VISIBLE,
									330, 605, 136, 40,
									Window,
									NULL,
									GetModuleHandle(NULL),
									NULL);

			HWND cboSize = CreateWindow("COMBOBOX",
									NULL,
									WS_CHILD | WS_VISIBLE | WS_TABSTOP | WS_VSCROLL | CBS_DROPDOWNLIST,
									330, 625, 136, 180,
									Window,
									(HMENU)IDD_SIZE_COMBO,
									GetModuleHandle(NULL),
									NULL);	

			//a button
			HWND btnStart = CreateWindow("BUTTON", 
									"Start",
									WS_CHILD | WS_VISIBLE | WS_TABSTOP | BS_DEFPUSHBUTTON, 
									516, 625, 70, 30,
									Window,
									(HMENU)IDC_START_BUTTON,
									GetModuleHandle(NULL),
									NULL);

			HWND lblGens = CreateWindow("STATIC",
									"Generations:\r\n",
									WS_CHILD | WS_VISIBLE,
									635, 620, 136, 40,
									Window,
									NULL,
									GetModuleHandle(NULL),
									NULL);

			HWND lblGensCount = CreateWindow("STATIC",
									"",
									WS_CHILD | WS_VISIBLE,
									635, 640, 150, 20,
									Window,
									(HMENU)IDC_GENS_LABEL,
									GetModuleHandle(NULL),
									NULL);

			//add combo values and defaults
			const char* sizes[] = {"Small","Medium","Large"};
			for(int i = 0; i < 3; ++i)
				SendMessage(cboSize, CB_ADDSTRING,NULL,reinterpret_cast<LPARAM>(sizes[i]));

			const char* patterns[] = {"Clear","Glider","Large exploder","10 cell row","Spaceship","Tumbler","10 cell grower","Glider Gun (A)","Glide Gun (B)"};
			for(int i = 0; i < 9; ++i)
				SendMessage(cboPatterns, CB_ADDSTRING,NULL,reinterpret_cast<LPARAM>(patterns[i]));

			SendMessage(cboSize, CB_SETCURSEL, 1, 0);//set default grid size (Medium)
			SendMessage(cboPatterns, CB_SETCURSEL, 0, 0);

			// Let's create an SFML view
			HWND View = CreateWindow("STATIC",
									NULL, WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS,
									3,  3, 793, 595,
									Window,
									(HMENU)IDS_SFML_STATIC, 
									GetModuleHandle(NULL), 
									NULL);
			
			return 0;
		}		

        // Quit when we close the main window
        case WM_CLOSE :
        {
            PostQuitMessage(0);
            return 0;
        }
		case WM_COMMAND:
		switch(LOWORD(WParam))
		{
			case IDC_START_BUTTON:
			{
				//I dont want 2 buttons, so change text instead
				if(run)
				{
					run = false;
					SendDlgItemMessage(Window, IDC_START_BUTTON, WM_SETTEXT, 0, (LPARAM)"Start");
				}
				else
				{
					run = true;
					SendDlgItemMessage(Window, IDC_START_BUTTON, WM_SETTEXT, 0, (LPARAM)"Stop");
				}
			}
			break;
			case IDD_SIZE_COMBO: // If the combo box sent the message,
			{
				switch(HIWORD(WParam)) // Find out what message it was
				{				
					case CBN_SELENDOK:
						//get selected index and set the grid size
						//stops running sim sets size and restarts if it was running
						int res = SendDlgItemMessage(Window,IDD_SIZE_COMBO,CB_GETCURSEL,WParam,LParam);
						if(res != CB_ERR)
						{
							bool wasRunning = run;
							switch(res)
							{
								
							case 0:
								run = false;
								grid->setGridSize(264, 198, 2);
								break;
							case 1:
								run = false;
								grid->setGridSize(132, 99, 5);
								break;
							case 2:
								run = false;
								grid->setGridSize(72, 54, 10);
								break;
							}
							run = wasRunning;
						}
					}
			}
            break;
			case IDD_PATTERNS_COMBO:
			{
				switch(HIWORD(WParam))
				{
				case CBN_SELENDOK:
					//get index of selected and apply that pattern
					int res = SendDlgItemMessage(Window,IDD_PATTERNS_COMBO,CB_GETCURSEL,WParam,LParam);
					if(res != CB_ERR)
					{
						grid->applyPattern((Patterns::Pattern)res);						
						SendDlgItemMessage(Window, IDC_GENS_LABEL, WM_SETTEXT, NULL, (LPARAM)grid->getGenerations().c_str());
					}
					break;
				}
			}
		}
		break;
		case WM_CTLCOLORSTATIC:
			
			HDC hdcStatic = (HDC) WParam; 
			SetTextColor(hdcStatic, RGB(0,0,0));  
			SetBkMode (hdcStatic, TRANSPARENT);

			return (LRESULT)hBrush;
		}

    return DefWindowProc(Window, Message, WParam, LParam);
}
