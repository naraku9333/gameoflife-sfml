#include "Cell.h"

GameOfLife::Cell::Cell():alive(false),sideLength(5)
{			
	rect = sf::RectangleShape(sf::Vector2f((float)sideLength, (float)sideLength));
	rect.setFillColor(sf::Color::White);
}

GameOfLife::Cell::Cell(int side):alive(false),sideLength(side)
{	
	rect = sf::RectangleShape(sf::Vector2f((float)sideLength, (float)sideLength));
	rect.setFillColor(sf::Color::White);
}		

void GameOfLife::Cell::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	target.draw(rect, states);
}

void GameOfLife::Cell::changeLife()
{
	alive = !alive;
	sf::Color c = (alive) ? sf::Color::Blue : sf::Color::White;
	rect.setFillColor(c);
}
void GameOfLife::Cell::setSize(const int size)
{
	sideLength = size;
	rect.setSize(sf::Vector2f((float)sideLength, (float)sideLength));
}