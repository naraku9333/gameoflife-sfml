#include "Patterns.h"
using GameOfLife::Patterns;

std::vector< std::vector<int> > GameOfLife::Patterns::patterns;

//TODO fix pattern arrays using 
//(0,0) as center of pattern (see glidergunA)
void GameOfLife::Patterns::set()
		{
			//clear
			//empty vector as placeholder, keeps indices synced with the enums
			std::vector<int> v;
			patterns.push_back(v);

			//glider
			int glider[] = {1,0,2,1,2,2,1,2,0,2};
			v.clear();
			v.assign(glider,glider + sizeof(glider)/sizeof(glider[0]));
			patterns.push_back(v);

			//lrg exploder
			int lgexploder[] = {0,0,0,1,0,2,0,3,0,4,2,0,2,4,4,0,4,1,4,2,4,3,4,4};
			v.clear();
			v.assign(lgexploder,lgexploder + sizeof(lgexploder)/sizeof(lgexploder[0]));
			patterns.push_back(v);

			//10 cell row
			int row10[] = {0,0,1,0,2,0,3,0,4,0,5,0,6,0,7,0,8,0,9,0};
			v.clear();
			v.assign(row10,row10 + sizeof(row10)/sizeof(row10[0]));
			patterns.push_back(v);

			//spaceship
			int spaceship[]  = {0,1,0,3,1,0,2,0,3,0,3,3,4,0,4,1,4,2};
			v.clear();
			v.assign(spaceship,spaceship + sizeof(spaceship)/sizeof(spaceship[0]));
			patterns.push_back(v);

			//tumbler
			int tumbler[]  = {0,3,0,4,0,5,1,0,1,1,1,5,2,0, 
							2,1,2,2,2,3,2,4,4,0,4,1,4,2, 
							4,3,4,4,5,0,5,1,5,5,6,3,6,4,6,5};
			v.clear();
			v.assign(tumbler,tumbler + sizeof(tumbler)/sizeof(tumbler[0]));
			patterns.push_back(v);

			//10 cell grower
			int grower10[] = {1,0,1,-1,3,-2,3,-1,4,-1,3,0,1,1,-1,2,-1,3,-3,3};
			v.clear();
			v.assign(grower10,grower10 + sizeof(grower10)/sizeof(grower10[0]));
			patterns.push_back(v);

			//glider gun A
			int glidergunA[] = {-18,0,-18,1,-17,0,-17,1,-7,0,-7,1,-7,2,-6,-1,-5,-2,-4,-2,-6,3,-5,4,-4,4,
								-3,1,-2,-1,-2,3,-1,0,-1,1,-1,2,0,1,3,0,3,-1,3,-2,4,0,4,-1,4,-2,5,1,5,-3,
								7,-4,7,-3,7,1,7,2,17,-2,17,-1,18,-2,18,-1}; 
			v.clear();
			v.assign(glidergunA,glidergunA + sizeof(glidergunA)/sizeof(glidergunA[0]));
			patterns.push_back(v);

			//glider gun B
			int glidergunB[] = {0,2,0,3,1,2,1,3,8,3,8,4,
							9,2,9,4,10,2,10,3,16,4,16,5,
							16,6,17,4,18,5,22,1,22,2,23,0, 
							23,2,24,0,24,1,24,12,24,13,25,12, 
							25,14,26,12,34,0,34,1,35,0,35,1, 
							35,7,35,8,35,9,36,7,37,8};
			v.clear();
			v.assign(glidergunB,glidergunB + sizeof(glidergunB)/sizeof(glidergunB[0]));
			patterns.push_back(v);
		}