/**
* TODO test different size grids and different systems!!!!	DONE
* TODO split header and implementation	DONE
* TODO maybe use std::vector< std::vector<Cell*> >  DONE
* TODO use x,y just for coords and row,col for grid indices
* TODO figure out how to track live cells so checking all isnt nessecary (std::vector<cell> liveCells;)
*/

#ifndef GOL_GRID_SV
#define GOL_GRID_SV
#include <vector>
#include "Cell.h"
#include "Patterns.h"

namespace GameOfLife
{
	class CellGrid
	{
		std::vector<std::vector<Cell>> cellGrid;
		//grid dimensions
		unsigned int width,
			height;
		unsigned int generations;//count the generations
		const int PAD;//border padding
		unsigned int sideLength;

	public:
		CellGrid():width(0), height(0),PAD(1),sideLength(0){}

		//create cell grid with given size [x][y]
		CellGrid(unsigned int x, unsigned int y,unsigned int side);

		//draw grid by passing RenderTarget to the drawable Cell
		void draw(sf::RenderTarget& target, sf::RenderStates states);

		//before checking if a cell is alive, 
		//check if its within bounds
		bool isValidCell(unsigned int i, unsigned int j);

		//run a generation of the game
		void runGeneration();

		//change a cells state (life) given the cells coord
		void changeCellState(sf::Vector2i& pos);		

		//count a cells living neighbors
		const int countNeighbors(int row, int col);

		//apply a preset pattern
		void applyPattern(Patterns::Pattern);

		//kill all living cells
		void clearGrid();

		//change the grid size
		void setGridSize(unsigned int x, unsigned int y, unsigned int side);
		
		std::string getGenerations();
	};
}
#endif
