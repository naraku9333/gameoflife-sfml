/**
* Cell class for GOL
*
*/
#ifndef GOL_CELL_SV
#define GOL_CELL_SV

#include <SFML\Graphics.hpp>

namespace GameOfLife
{
	class Cell : public sf::Drawable
	{		
		bool alive;
		sf::RectangleShape rect;
		int sideLength;

	public:		
		Cell();
		
		Cell(int side);
		
		//required implementation
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		
		//change cell's state
		void changeLife();
		
		bool isAlive() const {return alive;} 

		//accessors/mutators
		const int getSideLength(){return sideLength;}

		void setPosition(const sf::Vector2f& pos){rect.setPosition(pos);}

		void setSize(const int size);

		void setColor(const sf::Color& c){rect.setFillColor(c);}
	};
}
#endif
