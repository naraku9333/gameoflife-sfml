//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by script.rc
//
#define IDD_SIZE_COMBO                  101
#define IDI_ICON1                       102
#define IDC_START_BUTTON				103
#define IDC_GENS_LABEL					104
#define IDD_PATTERNS_COMBO				105
#define IDS_SFML_STATIC					106

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        103
#define _APS_NEXT_COMMAND_VALUE         40002
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
